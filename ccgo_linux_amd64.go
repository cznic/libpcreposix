// Code generated for linux/amd64 by 'generator --libc modernc.org/libc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -o libpcreposix.go --package-name libpcreposix .libs/libpcreposix.a -lpcre', DO NOT EDIT.

//go:build linux && amd64

package libpcreposix

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libpcre"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_ARG_MAX = 131072
const m_BC_BASE_MAX = 99
const m_BC_DIM_MAX = 2048
const m_BC_SCALE_MAX = 99
const m_BC_STRING_MAX = 1000
const m_BUFSIZ = 1024
const m_CHARCLASS_NAME_MAX = 14
const m_CHAR_0 = '\060'
const m_CHAR_1 = '\061'
const m_CHAR_2 = '\062'
const m_CHAR_3 = '\063'
const m_CHAR_4 = '\064'
const m_CHAR_5 = '\065'
const m_CHAR_6 = '\066'
const m_CHAR_7 = '\067'
const m_CHAR_8 = '\070'
const m_CHAR_9 = '\071'
const m_CHAR_A = '\101'
const m_CHAR_AMPERSAND = '\046'
const m_CHAR_APOSTROPHE = '\047'
const m_CHAR_ASTERISK = '\052'
const m_CHAR_B = '\102'
const m_CHAR_BACKSLASH = '\134'
const m_CHAR_BEL = '\007'
const m_CHAR_BIT = 8
const m_CHAR_BS = '\010'
const m_CHAR_C = '\103'
const m_CHAR_CIRCUMFLEX_ACCENT = '\136'
const m_CHAR_COLON = '\072'
const m_CHAR_COMMA = '\054'
const m_CHAR_COMMERCIAL_AT = '\100'
const m_CHAR_CR = '\015'
const m_CHAR_D = '\104'
const m_CHAR_DEL = '\177'
const m_CHAR_DOLLAR_SIGN = '\044'
const m_CHAR_DOT = '\056'
const m_CHAR_E = '\105'
const m_CHAR_EQUALS_SIGN = '\075'
const m_CHAR_ESC = '\033'
const m_CHAR_EXCLAMATION_MARK = '\041'
const m_CHAR_F = '\106'
const m_CHAR_FF = '\014'
const m_CHAR_G = '\107'
const m_CHAR_GRAVE_ACCENT = '\140'
const m_CHAR_GREATER_THAN_SIGN = '\076'
const m_CHAR_H = '\110'
const m_CHAR_HT = '\011'
const m_CHAR_I = '\111'
const m_CHAR_J = '\112'
const m_CHAR_K = '\113'
const m_CHAR_L = '\114'
const m_CHAR_LEFT_CURLY_BRACKET = '\173'
const m_CHAR_LEFT_PARENTHESIS = '\050'
const m_CHAR_LEFT_SQUARE_BRACKET = '\133'
const m_CHAR_LESS_THAN_SIGN = '\074'
const m_CHAR_LF = '\012'
const m_CHAR_M = '\115'
const m_CHAR_MAX = 255
const m_CHAR_MIN = 0
const m_CHAR_MINUS = '\055'
const m_CHAR_N = '\116'
const m_CHAR_NL = "CHAR_LF"
const m_CHAR_NUMBER_SIGN = '\043'
const m_CHAR_O = '\117'
const m_CHAR_P = '\120'
const m_CHAR_PERCENT_SIGN = '\045'
const m_CHAR_PLUS = '\053'
const m_CHAR_Q = '\121'
const m_CHAR_QUESTION_MARK = '\077'
const m_CHAR_QUOTATION_MARK = '\042'
const m_CHAR_R = '\122'
const m_CHAR_RIGHT_CURLY_BRACKET = '\175'
const m_CHAR_RIGHT_PARENTHESIS = '\051'
const m_CHAR_RIGHT_SQUARE_BRACKET = '\135'
const m_CHAR_S = '\123'
const m_CHAR_SEMICOLON = '\073'
const m_CHAR_SLASH = '\057'
const m_CHAR_SPACE = '\040'
const m_CHAR_T = '\124'
const m_CHAR_TILDE = '\176'
const m_CHAR_U = '\125'
const m_CHAR_UNDERSCORE = '\137'
const m_CHAR_V = '\126'
const m_CHAR_VERTICAL_LINE = '\174'
const m_CHAR_VT = '\013'
const m_CHAR_W = '\127'
const m_CHAR_X = '\130'
const m_CHAR_Y = '\131'
const m_CHAR_Z = '\132'
const m_CHAR_a = '\141'
const m_CHAR_b = '\142'
const m_CHAR_c = '\143'
const m_CHAR_d = '\144'
const m_CHAR_e = '\145'
const m_CHAR_f = '\146'
const m_CHAR_g = '\147'
const m_CHAR_h = '\150'
const m_CHAR_i = '\151'
const m_CHAR_j = '\152'
const m_CHAR_k = '\153'
const m_CHAR_l = '\154'
const m_CHAR_m = '\155'
const m_CHAR_n = '\156'
const m_CHAR_o = '\157'
const m_CHAR_p = '\160'
const m_CHAR_q = '\161'
const m_CHAR_r = '\162'
const m_CHAR_s = '\163'
const m_CHAR_t = '\164'
const m_CHAR_u = '\165'
const m_CHAR_v = '\166'
const m_CHAR_w = '\167'
const m_CHAR_x = '\170'
const m_CHAR_y = '\171'
const m_CHAR_z = '\172'
const m_COLL_WEIGHTS_MAX = 2
const m_DELAYTIMER_MAX = 0x7fffffff
const m_ESC_a = "CHAR_BEL"
const m_ESC_e = "CHAR_ESC"
const m_ESC_f = "CHAR_FF"
const m_ESC_n = "CHAR_LF"
const m_ESC_r = "CHAR_CR"
const m_ESC_tee = "CHAR_HT"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_EXPR_NEST_MAX = 32
const m_FALSE = 0
const m_FILENAME_MAX = 4096
const m_FILESIZEBITS = 64
const m_FIRST_AUTOTAB_OP = "OP_NOT_DIGIT"
const m_FOPEN_MAX = 1000
const m_HAVE_BCOPY = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DIRENT_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LIMITS_H = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRERROR = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_HAVE_ZLIB_H = 1
const m_HOST_NAME_MAX = 255
const m_IMM2_SIZE = 2
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT64_OR_DOUBLE = "int64_t"
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_INT_MAX = 2147483647
const m_IOV_MAX = 1024
const m_LAST_AUTOTAB_LEFT_OP = "OP_EXTUNI"
const m_LAST_AUTOTAB_RIGHT_OP = "OP_DOLLM"
const m_LINE_MAX = 4096
const m_LINK_SIZE = 2
const m_LLONG_MAX = 0x7fffffffffffffff
const m_LOGIN_NAME_MAX = 256
const m_LONG_BIT = 64
const m_LONG_MAX = "__LONG_MAX"
const m_LT_OBJDIR = ".libs/"
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_MAGIC_NUMBER = 0x50435245
const m_MATCH_LIMIT = 10000000
const m_MATCH_LIMIT_RECURSION = "MATCH_LIMIT"
const m_MAX_NAME_COUNT = 10000
const m_MAX_NAME_SIZE = 32
const m_MAX_VALUE_FOR_SINGLE_CHAR = 127
const m_MB_LEN_MAX = 4
const m_MQ_PRIO_MAX = 32768
const m_NAME_MAX = 255
const m_NEWLINE = 10
const m_NGROUPS_MAX = 32
const m_NLTYPE_ANY = 1
const m_NLTYPE_ANYCRLF = 2
const m_NLTYPE_FIXED = 0
const m_NL_ARGMAX = 9
const m_NL_LANGMAX = 32
const m_NL_MSGMAX = 32767
const m_NL_NMAX = 16
const m_NL_SETMAX = 255
const m_NL_TEXTMAX = 2048
const m_NOTACHAR = 0xffffffff
const m_NZERO = 20
const m_PACKAGE = "pcre"
const m_PACKAGE_BUGREPORT = ""
const m_PACKAGE_NAME = "PCRE"
const m_PACKAGE_STRING = "PCRE 8.45"
const m_PACKAGE_TARNAME = "pcre"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "8.45"
const m_PAGESIZE = 4096
const m_PAGE_SIZE = "PAGESIZE"
const m_PARENS_NEST_LIMIT = 250
const m_PATH_MAX = 4096
const m_PCREGREP_BUFSIZE = 20480
const m_PCRE_ANCHORED = 0x00000010
const m_PCRE_AUTO_CALLOUT = 0x00004000
const m_PCRE_BSR_ANYCRLF = 0x00800000
const m_PCRE_BSR_UNICODE = 0x01000000
const m_PCRE_CASELESS = 1
const m_PCRE_CONFIG_BSR = 8
const m_PCRE_CONFIG_JIT = 9
const m_PCRE_CONFIG_JITTARGET = 11
const m_PCRE_CONFIG_LINK_SIZE = 2
const m_PCRE_CONFIG_MATCH_LIMIT = 4
const m_PCRE_CONFIG_MATCH_LIMIT_RECURSION = 7
const m_PCRE_CONFIG_NEWLINE = 1
const m_PCRE_CONFIG_PARENS_LIMIT = 13
const m_PCRE_CONFIG_POSIX_MALLOC_THRESHOLD = 3
const m_PCRE_CONFIG_STACKRECURSE = 5
const m_PCRE_CONFIG_UNICODE_PROPERTIES = 6
const m_PCRE_CONFIG_UTF16 = 10
const m_PCRE_CONFIG_UTF32 = 12
const m_PCRE_CONFIG_UTF8 = 0
const m_PCRE_DFA_RESTART = 0x00020000
const m_PCRE_DFA_SHORTEST = 0x00010000
const m_PCRE_DOLLAR_ENDONLY = 0x00000020
const m_PCRE_DOTALL = 4
const m_PCRE_DUPNAMES = 0x00080000
const m_PCRE_EXTENDED = 0x00000008
const m_PCRE_EXTRA = 0x00000040
const m_PCRE_EXTRA_CALLOUT_DATA = 0x0004
const m_PCRE_EXTRA_EXECUTABLE_JIT = 0x0040
const m_PCRE_EXTRA_MARK = 0x0020
const m_PCRE_EXTRA_MATCH_LIMIT = 0x0002
const m_PCRE_EXTRA_MATCH_LIMIT_RECURSION = 0x0010
const m_PCRE_EXTRA_STUDY_DATA = 0x0001
const m_PCRE_EXTRA_TABLES = 0x0008
const m_PCRE_FCH_CASELESS = 0x00000020
const m_PCRE_FIRSTLINE = 0x00040000
const m_PCRE_FIRSTSET = 0x00000010
const m_PCRE_HASCRORLF = 0x00000800
const m_PCRE_HASTHEN = 0x00001000
const m_PCRE_INFO_BACKREFMAX = 3
const m_PCRE_INFO_CAPTURECOUNT = 2
const m_PCRE_INFO_DEFAULT_TABLES = 11
const m_PCRE_INFO_FIRSTBYTE = 4
const m_PCRE_INFO_FIRSTCHAR = 4
const m_PCRE_INFO_FIRSTCHARACTER = 19
const m_PCRE_INFO_FIRSTCHARACTERFLAGS = 20
const m_PCRE_INFO_FIRSTTABLE = 5
const m_PCRE_INFO_HASCRORLF = 14
const m_PCRE_INFO_JCHANGED = 13
const m_PCRE_INFO_JIT = 16
const m_PCRE_INFO_JITSIZE = 17
const m_PCRE_INFO_LASTLITERAL = 6
const m_PCRE_INFO_MATCHLIMIT = 23
const m_PCRE_INFO_MATCH_EMPTY = 25
const m_PCRE_INFO_MAXLOOKBEHIND = 18
const m_PCRE_INFO_MINLENGTH = 15
const m_PCRE_INFO_NAMECOUNT = 8
const m_PCRE_INFO_NAMEENTRYSIZE = 7
const m_PCRE_INFO_NAMETABLE = 9
const m_PCRE_INFO_OKPARTIAL = 12
const m_PCRE_INFO_OPTIONS = 0
const m_PCRE_INFO_RECURSIONLIMIT = 24
const m_PCRE_INFO_REQUIREDCHAR = 21
const m_PCRE_INFO_REQUIREDCHARFLAGS = 22
const m_PCRE_INFO_SIZE = 1
const m_PCRE_INFO_STUDYSIZE = 10
const m_PCRE_INT16_MAX = "SHRT_MAX"
const m_PCRE_INT32_MAX = "INT_MAX"
const m_PCRE_JAVASCRIPT_COMPAT = 0x02000000
const m_PCRE_JCHANGED = 0x00000400
const m_PCRE_MAJOR = 8
const m_PCRE_MATCH_EMPTY = 0x00008000
const m_PCRE_MINOR = 45
const m_PCRE_MLSET = 0x00002000
const m_PCRE_MODE = "PCRE_MODE8"
const m_PCRE_MODE16 = 0x00000002
const m_PCRE_MODE32 = 0x00000004
const m_PCRE_MODE8 = 0x00000001
const m_PCRE_MULTILINE = 2
const m_PCRE_NEVER_UTF = 0x00010000
const m_PCRE_NEWLINE_ANY = 0x00400000
const m_PCRE_NEWLINE_ANYCRLF = 0x00500000
const m_PCRE_NEWLINE_CR = 0x00100000
const m_PCRE_NEWLINE_CRLF = 0x00300000
const m_PCRE_NEWLINE_LF = 0x00200000
const m_PCRE_NOPARTIAL = 0x00000200
const m_PCRE_NOTBOL = 128
const m_PCRE_NOTEMPTY = 1024
const m_PCRE_NOTEMPTY_ATSTART = 0x10000000
const m_PCRE_NOTEOL = 256
const m_PCRE_NO_AUTO_CAPTURE = 4096
const m_PCRE_NO_AUTO_POSSESS = 0x00020000
const m_PCRE_NO_START_OPTIMISE = 0x04000000
const m_PCRE_NO_START_OPTIMIZE = 0x04000000
const m_PCRE_NO_UTF16_CHECK = 0x00002000
const m_PCRE_NO_UTF32_CHECK = 0x00002000
const m_PCRE_NO_UTF8_CHECK = 0x00002000
const m_PCRE_PARTIAL = 0x00008000
const m_PCRE_PARTIAL_HARD = 0x08000000
const m_PCRE_PARTIAL_SOFT = 0x00008000
const m_PCRE_RCH_CASELESS = 0x00000080
const m_PCRE_REQCHSET = 0x00000040
const m_PCRE_RLSET = 0x00004000
const m_PCRE_STARTLINE = 0x00000100
const m_PCRE_STATIC = 1
const m_PCRE_STUDY_EXTRA_NEEDED = 0x0008
const m_PCRE_STUDY_JIT_COMPILE = 0x0001
const m_PCRE_STUDY_JIT_PARTIAL_HARD_COMPILE = 0x0004
const m_PCRE_STUDY_JIT_PARTIAL_SOFT_COMPILE = 0x0002
const m_PCRE_STUDY_MAPPED = 0x0001
const m_PCRE_STUDY_MINLEN = 0x0002
const m_PCRE_UCP = 536870912
const m_PCRE_UINT16_MAX = "USHRT_MAX"
const m_PCRE_UINT32_MAX = "UINT_MAX"
const m_PCRE_UNGREEDY = 512
const m_PCRE_UTF16 = 0x00000800
const m_PCRE_UTF16_ERR0 = 0
const m_PCRE_UTF16_ERR1 = 1
const m_PCRE_UTF16_ERR2 = 2
const m_PCRE_UTF16_ERR3 = 3
const m_PCRE_UTF16_ERR4 = 4
const m_PCRE_UTF32 = 0x00000800
const m_PCRE_UTF32_ERR0 = 0
const m_PCRE_UTF32_ERR1 = 1
const m_PCRE_UTF32_ERR2 = 2
const m_PCRE_UTF32_ERR3 = 3
const m_PCRE_UTF8 = 2048
const m_PCRE_UTF8_ERR0 = 0
const m_PCRE_UTF8_ERR1 = 1
const m_PCRE_UTF8_ERR10 = 10
const m_PCRE_UTF8_ERR11 = 11
const m_PCRE_UTF8_ERR12 = 12
const m_PCRE_UTF8_ERR13 = 13
const m_PCRE_UTF8_ERR14 = 14
const m_PCRE_UTF8_ERR15 = 15
const m_PCRE_UTF8_ERR16 = 16
const m_PCRE_UTF8_ERR17 = 17
const m_PCRE_UTF8_ERR18 = 18
const m_PCRE_UTF8_ERR19 = 19
const m_PCRE_UTF8_ERR2 = 2
const m_PCRE_UTF8_ERR20 = 20
const m_PCRE_UTF8_ERR21 = 21
const m_PCRE_UTF8_ERR22 = 22
const m_PCRE_UTF8_ERR3 = 3
const m_PCRE_UTF8_ERR4 = 4
const m_PCRE_UTF8_ERR5 = 5
const m_PCRE_UTF8_ERR6 = 6
const m_PCRE_UTF8_ERR7 = 7
const m_PCRE_UTF8_ERR8 = 8
const m_PCRE_UTF8_ERR9 = 9
const m_PIPE_BUF = 4096
const m_POSIX_MALLOC_THRESHOLD = 10
const m_PTHREAD_DESTRUCTOR_ITERATIONS = 4
const m_PTHREAD_KEYS_MAX = 128
const m_PTHREAD_STACK_MIN = 2048
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_PT_ALNUM = 5
const m_PT_ANY = 0
const m_PT_CLIST = 9
const m_PT_GC = 2
const m_PT_LAMP = 1
const m_PT_PC = 3
const m_PT_PXGRAPH = 11
const m_PT_PXPRINT = 12
const m_PT_PXPUNCT = 13
const m_PT_PXSPACE = 7
const m_PT_SC = 4
const m_PT_SPACE = 6
const m_PT_TABSIZE = 11
const m_PT_UCNC = 10
const m_PT_WORD = 8
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 0x7fffffff
const m_REAL_PCRE = "real_pcre"
const m_REG_DOTALL = 16
const m_REG_EXTENDED = 0
const m_REG_ICASE = 1
const m_REG_NEWLINE = 2
const m_REG_NOSUB = 32
const m_REG_NOTBOL = 4
const m_REG_NOTEMPTY = 256
const m_REG_NOTEOL = 8
const m_REG_STARTEND = 128
const m_REG_UCP = 1024
const m_REG_UNGREEDY = 512
const m_REG_UTF8 = 64
const m_REQ_BYTE_MAX = 1000
const m_REVERSED_MAGIC_NUMBER = 0x45524350
const m_RE_DUP_MAX = 255
const m_RREF_ANY = 0xffff
const m_SCHAR_MAX = 127
const m_SEM_NSEMS_MAX = 256
const m_SEM_VALUE_MAX = 0x7fffffff
const m_SHRT_MAX = 0x7fff
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIZE_MAX = "UINT64_MAX"
const m_SSIZE_MAX = "LONG_MAX"
const m_STDC_HEADERS = 1
const m_STR_0 = "\\060"
const m_STR_1 = "\\061"
const m_STR_2 = "\\062"
const m_STR_3 = "\\063"
const m_STR_4 = "\\064"
const m_STR_5 = "\\065"
const m_STR_6 = "\\066"
const m_STR_7 = "\\067"
const m_STR_8 = "\\070"
const m_STR_9 = "\\071"
const m_STR_A = "\\101"
const m_STR_AMPERSAND = "\\046"
const m_STR_APOSTROPHE = "\\047"
const m_STR_ASTERISK = "\\052"
const m_STR_B = "\\102"
const m_STR_BACKSLASH = "\\134"
const m_STR_BEL = "\\007"
const m_STR_BS = "\\010"
const m_STR_C = "\\103"
const m_STR_CIRCUMFLEX_ACCENT = "\\136"
const m_STR_COLON = "\\072"
const m_STR_COMMA = "\\054"
const m_STR_COMMERCIAL_AT = "\\100"
const m_STR_CR = "\\015"
const m_STR_D = "\\104"
const m_STR_DEL = "\\177"
const m_STR_DOLLAR_SIGN = "\\044"
const m_STR_DOT = "\\056"
const m_STR_E = "\\105"
const m_STR_EQUALS_SIGN = "\\075"
const m_STR_ESC = "\\033"
const m_STR_EXCLAMATION_MARK = "\\041"
const m_STR_F = "\\106"
const m_STR_FF = "\\014"
const m_STR_G = "\\107"
const m_STR_GRAVE_ACCENT = "\\140"
const m_STR_GREATER_THAN_SIGN = "\\076"
const m_STR_H = "\\110"
const m_STR_HT = "\\011"
const m_STR_I = "\\111"
const m_STR_J = "\\112"
const m_STR_K = "\\113"
const m_STR_L = "\\114"
const m_STR_LEFT_CURLY_BRACKET = "\\173"
const m_STR_LEFT_PARENTHESIS = "\\050"
const m_STR_LEFT_SQUARE_BRACKET = "\\133"
const m_STR_LESS_THAN_SIGN = "\\074"
const m_STR_M = "\\115"
const m_STR_MINUS = "\\055"
const m_STR_N = "\\116"
const m_STR_NL = "\\012"
const m_STR_NUMBER_SIGN = "\\043"
const m_STR_O = "\\117"
const m_STR_P = "\\120"
const m_STR_PERCENT_SIGN = "\\045"
const m_STR_PLUS = "\\053"
const m_STR_Q = "\\121"
const m_STR_QUESTION_MARK = "\\077"
const m_STR_QUOTATION_MARK = "\\042"
const m_STR_R = "\\122"
const m_STR_RIGHT_CURLY_BRACKET = "\\175"
const m_STR_RIGHT_PARENTHESIS = "\\051"
const m_STR_RIGHT_SQUARE_BRACKET = "\\135"
const m_STR_S = "\\123"
const m_STR_SEMICOLON = "\\073"
const m_STR_SLASH = "\\057"
const m_STR_SPACE = "\\040"
const m_STR_T = "\\124"
const m_STR_TILDE = "\\176"
const m_STR_U = "\\125"
const m_STR_UNDERSCORE = "\\137"
const m_STR_V = "\\126"
const m_STR_VERTICAL_LINE = "\\174"
const m_STR_VT = "\\013"
const m_STR_W = "\\127"
const m_STR_X = "\\130"
const m_STR_Y = "\\131"
const m_STR_Z = "\\132"
const m_STR_a = "\\141"
const m_STR_b = "\\142"
const m_STR_c = "\\143"
const m_STR_d = "\\144"
const m_STR_e = "\\145"
const m_STR_f = "\\146"
const m_STR_g = "\\147"
const m_STR_h = "\\150"
const m_STR_i = "\\151"
const m_STR_j = "\\152"
const m_STR_k = "\\153"
const m_STR_l = "\\154"
const m_STR_m = "\\155"
const m_STR_n = "\\156"
const m_STR_o = "\\157"
const m_STR_p = "\\160"
const m_STR_q = "\\161"
const m_STR_r = "\\162"
const m_STR_s = "\\163"
const m_STR_t = "\\164"
const m_STR_u = "\\165"
const m_STR_v = "\\166"
const m_STR_w = "\\167"
const m_STR_x = "\\170"
const m_STR_y = "\\171"
const m_STR_z = "\\172"
const m_SUPPORT_UTF8 = 1
const m_SYMLOOP_MAX = 40
const m_TMP_MAX = 10000
const m_TRUE = 1
const m_TTY_NAME_MAX = 32
const m_TZNAME_MAX = 6
const m_UCD_BLOCK_SIZE = 128
const m_UCHAR_MAX = 255
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UINT_MAX = 0xffffffff
const m_USHRT_MAX = 0xffff
const m_VERSION = "8.45"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WORD_BIT = 32
const m_WUNTRACED = 2
const m_XCL_END = 0
const m_XCL_HASPROP = 0x04
const m_XCL_MAP = 0x02
const m_XCL_NOT = 0x01
const m_XCL_NOTPROP = 4
const m_XCL_PROP = 3
const m_XCL_RANGE = 2
const m_XCL_SINGLE = 1
const m__GNU_SOURCE = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m__POSIX2_BC_BASE_MAX = 99
const m__POSIX2_BC_DIM_MAX = 2048
const m__POSIX2_BC_SCALE_MAX = 99
const m__POSIX2_BC_STRING_MAX = 1000
const m__POSIX2_CHARCLASS_NAME_MAX = 14
const m__POSIX2_COLL_WEIGHTS_MAX = 2
const m__POSIX2_EXPR_NEST_MAX = 32
const m__POSIX2_LINE_MAX = 2048
const m__POSIX2_RE_DUP_MAX = 255
const m__POSIX_AIO_LISTIO_MAX = 2
const m__POSIX_AIO_MAX = 1
const m__POSIX_ARG_MAX = 4096
const m__POSIX_CHILD_MAX = 25
const m__POSIX_CLOCKRES_MIN = 20000000
const m__POSIX_DELAYTIMER_MAX = 32
const m__POSIX_HOST_NAME_MAX = 255
const m__POSIX_LINK_MAX = 8
const m__POSIX_LOGIN_NAME_MAX = 9
const m__POSIX_MAX_CANON = 255
const m__POSIX_MAX_INPUT = 255
const m__POSIX_MQ_OPEN_MAX = 8
const m__POSIX_MQ_PRIO_MAX = 32
const m__POSIX_NAME_MAX = 14
const m__POSIX_NGROUPS_MAX = 8
const m__POSIX_OPEN_MAX = 20
const m__POSIX_PATH_MAX = 256
const m__POSIX_PIPE_BUF = 512
const m__POSIX_RE_DUP_MAX = 255
const m__POSIX_RTSIG_MAX = 8
const m__POSIX_SEM_NSEMS_MAX = 256
const m__POSIX_SEM_VALUE_MAX = 32767
const m__POSIX_SIGQUEUE_MAX = 32
const m__POSIX_SSIZE_MAX = 32767
const m__POSIX_SS_REPL_MAX = 4
const m__POSIX_STREAM_MAX = 8
const m__POSIX_SYMLINK_MAX = 255
const m__POSIX_SYMLOOP_MAX = 8
const m__POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_THREADS_MAX = 64
const m__POSIX_TIMER_MAX = 32
const m__POSIX_TRACE_EVENT_NAME_MAX = 30
const m__POSIX_TRACE_NAME_MAX = 8
const m__POSIX_TRACE_SYS_MAX = 8
const m__POSIX_TRACE_USER_EVENT_MAX = 32
const m__POSIX_TTY_NAME_MAX = 9
const m__POSIX_TZNAME_MAX = 6
const m__STDC_PREDEF_H = 1
const m__XOPEN_IOV_MAX = 16
const m__XOPEN_NAME_MAX = 255
const m__XOPEN_PATH_MAX = 1024
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_HLE_ACQUIRE = 65536
const m___ATOMIC_HLE_RELEASE = 131072
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 2
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_BID_FORMAT__ = 1
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 2
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_IS_IEC_60559__ = 2
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const m___FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 2
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 2
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 2
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 2
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 2
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_CONSTRUCTIVE_SIZE = 64
const m___GCC_DESTRUCTIVE_SIZE = 64
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 12
const m___GXX_ABI_VERSION = 1017
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 2
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const m___LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LITTLE_ENDIAN = 1234
const m___LONG_DOUBLE_64__ = 1
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MMX_WITH_SSE__ = 1
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT128__ = 16
const m___SIZEOF_FLOAT80__ = 16
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VERSION__ = "12.2.0"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___amd64 = 1
const m___amd64__ = 1
const m___code_model_small__ = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___k8 = 1
const m___k8__ = 1
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___unix = 1
const m___unix__ = 1
const m___x86_64 = 1
const m___x86_64__ = 1
const m_alloca = "__builtin_alloca"
const m_cbit_cntrl = 288
const m_cbit_digit = 64
const m_cbit_graph = 192
const m_cbit_length = 320
const m_cbit_lower = 128
const m_cbit_print = 224
const m_cbit_punct = 256
const m_cbit_space = 0
const m_cbit_upper = 96
const m_cbit_word = 160
const m_cbit_xdigit = 32
const m_cbits_offset = 512
const m_ctype_digit = 0x04
const m_ctype_letter = 0x02
const m_ctype_meta = 0x80
const m_ctype_space = 0x01
const m_ctype_word = 0x10
const m_ctype_xdigit = 0x08
const m_fcc_offset = 256
const m_lcc_offset = 0
const m_linux = 1
const m_unix = 1

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type Twchar_t = int32

type Tsize_t = uint64

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Treal_pcre8_or_161 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Tpcre = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Tpcre16 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Treal_pcre321 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint32
	Freq_char          Tpcre_uint32
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy             Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Tpcre32 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint32
	Freq_char          Tpcre_uint32
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy             Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Tpcre_extra = struct {
	Fflags                 uint64
	Fstudy_data            uintptr
	Fmatch_limit           uint64
	Fcallout_data          uintptr
	Ftables                uintptr
	Fmatch_limit_recursion uint64
	Fmark                  uintptr
	Fexecutable_jit        uintptr
}

type Tpcre16_extra = struct {
	Fflags                 uint64
	Fstudy_data            uintptr
	Fmatch_limit           uint64
	Fcallout_data          uintptr
	Ftables                uintptr
	Fmatch_limit_recursion uint64
	Fmark                  uintptr
	Fexecutable_jit        uintptr
}

type Tpcre32_extra = struct {
	Fflags                 uint64
	Fstudy_data            uintptr
	Fmatch_limit           uint64
	Fcallout_data          uintptr
	Ftables                uintptr
	Fmatch_limit_recursion uint64
	Fmark                  uintptr
	Fexecutable_jit        uintptr
}

type Tpcre_callout_block = struct {
	Fversion          int32
	Fcallout_number   int32
	Foffset_vector    uintptr
	Fsubject          uintptr
	Fsubject_length   int32
	Fstart_match      int32
	Fcurrent_position int32
	Fcapture_top      int32
	Fcapture_last     int32
	Fcallout_data     uintptr
	Fpattern_position int32
	Fnext_item_length int32
	Fmark             uintptr
}

type Tpcre16_callout_block = struct {
	Fversion          int32
	Fcallout_number   int32
	Foffset_vector    uintptr
	Fsubject          uintptr
	Fsubject_length   int32
	Fstart_match      int32
	Fcurrent_position int32
	Fcapture_top      int32
	Fcapture_last     int32
	Fcallout_data     uintptr
	Fpattern_position int32
	Fnext_item_length int32
	Fmark             uintptr
}

type Tpcre32_callout_block = struct {
	Fversion          int32
	Fcallout_number   int32
	Foffset_vector    uintptr
	Fsubject          uintptr
	Fsubject_length   int32
	Fstart_match      int32
	Fcurrent_position int32
	Fcapture_top      int32
	Fcapture_last     int32
	Fcallout_data     uintptr
	Fpattern_position int32
	Fnext_item_length int32
	Fmark             uintptr
}

type Tpcre_jit_callback = uintptr

type Tpcre16_jit_callback = uintptr

type Tpcre32_jit_callback = uintptr

type Tlocale_t = uintptr

type Tmax_align_t = struct {
	F__ll int64
	F__ld float64
}

type Tptrdiff_t = int64

type Tssize_t = int64

type Toff_t = int64

type Tva_list = uintptr

type t__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]int8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tpcre_uint8 = uint8

type Tpcre_uint16 = uint16

type Tpcre_int16 = int16

type Tpcre_uint32 = uint32

type Tpcre_int32 = int32

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Tpcre_uchar = uint8

const _ucp_C = 0
const _ucp_L = 1
const _ucp_M = 2
const _ucp_N = 3
const _ucp_P = 4
const _ucp_S = 5
const _ucp_Z = 6
const _ucp_Cc = 0
const _ucp_Cf = 1
const _ucp_Cn = 2
const _ucp_Co = 3
const _ucp_Cs = 4
const _ucp_Ll = 5
const _ucp_Lm = 6
const _ucp_Lo = 7
const _ucp_Lt = 8
const _ucp_Lu = 9
const _ucp_Mc = 10
const _ucp_Me = 11
const _ucp_Mn = 12
const _ucp_Nd = 13
const _ucp_Nl = 14
const _ucp_No = 15
const _ucp_Pc = 16
const _ucp_Pd = 17
const _ucp_Pe = 18
const _ucp_Pf = 19
const _ucp_Pi = 20
const _ucp_Po = 21
const _ucp_Ps = 22
const _ucp_Sc = 23
const _ucp_Sk = 24
const _ucp_Sm = 25
const _ucp_So = 26
const _ucp_Zl = 27
const _ucp_Zp = 28
const _ucp_Zs = 29
const _ucp_gbCR = 0
const _ucp_gbLF = 1
const _ucp_gbControl = 2
const _ucp_gbExtend = 3
const _ucp_gbPrepend = 4
const _ucp_gbSpacingMark = 5
const _ucp_gbL = 6
const _ucp_gbV = 7
const _ucp_gbT = 8
const _ucp_gbLV = 9
const _ucp_gbLVT = 10
const _ucp_gbRegionalIndicator = 11
const _ucp_gbOther = 12
const _ucp_Arabic = 0
const _ucp_Armenian = 1
const _ucp_Bengali = 2
const _ucp_Bopomofo = 3
const _ucp_Braille = 4
const _ucp_Buginese = 5
const _ucp_Buhid = 6
const _ucp_Canadian_Aboriginal = 7
const _ucp_Cherokee = 8
const _ucp_Common = 9
const _ucp_Coptic = 10
const _ucp_Cypriot = 11
const _ucp_Cyrillic = 12
const _ucp_Deseret = 13
const _ucp_Devanagari = 14
const _ucp_Ethiopic = 15
const _ucp_Georgian = 16
const _ucp_Glagolitic = 17
const _ucp_Gothic = 18
const _ucp_Greek = 19
const _ucp_Gujarati = 20
const _ucp_Gurmukhi = 21
const _ucp_Han = 22
const _ucp_Hangul = 23
const _ucp_Hanunoo = 24
const _ucp_Hebrew = 25
const _ucp_Hiragana = 26
const _ucp_Inherited = 27
const _ucp_Kannada = 28
const _ucp_Katakana = 29
const _ucp_Kharoshthi = 30
const _ucp_Khmer = 31
const _ucp_Lao = 32
const _ucp_Latin = 33
const _ucp_Limbu = 34
const _ucp_Linear_B = 35
const _ucp_Malayalam = 36
const _ucp_Mongolian = 37
const _ucp_Myanmar = 38
const _ucp_New_Tai_Lue = 39
const _ucp_Ogham = 40
const _ucp_Old_Italic = 41
const _ucp_Old_Persian = 42
const _ucp_Oriya = 43
const _ucp_Osmanya = 44
const _ucp_Runic = 45
const _ucp_Shavian = 46
const _ucp_Sinhala = 47
const _ucp_Syloti_Nagri = 48
const _ucp_Syriac = 49
const _ucp_Tagalog = 50
const _ucp_Tagbanwa = 51
const _ucp_Tai_Le = 52
const _ucp_Tamil = 53
const _ucp_Telugu = 54
const _ucp_Thaana = 55
const _ucp_Thai = 56
const _ucp_Tibetan = 57
const _ucp_Tifinagh = 58
const _ucp_Ugaritic = 59
const _ucp_Yi = 60
const _ucp_Balinese = 61
const _ucp_Cuneiform = 62
const _ucp_Nko = 63
const _ucp_Phags_Pa = 64
const _ucp_Phoenician = 65
const _ucp_Carian = 66
const _ucp_Cham = 67
const _ucp_Kayah_Li = 68
const _ucp_Lepcha = 69
const _ucp_Lycian = 70
const _ucp_Lydian = 71
const _ucp_Ol_Chiki = 72
const _ucp_Rejang = 73
const _ucp_Saurashtra = 74
const _ucp_Sundanese = 75
const _ucp_Vai = 76
const _ucp_Avestan = 77
const _ucp_Bamum = 78
const _ucp_Egyptian_Hieroglyphs = 79
const _ucp_Imperial_Aramaic = 80
const _ucp_Inscriptional_Pahlavi = 81
const _ucp_Inscriptional_Parthian = 82
const _ucp_Javanese = 83
const _ucp_Kaithi = 84
const _ucp_Lisu = 85
const _ucp_Meetei_Mayek = 86
const _ucp_Old_South_Arabian = 87
const _ucp_Old_Turkic = 88
const _ucp_Samaritan = 89
const _ucp_Tai_Tham = 90
const _ucp_Tai_Viet = 91
const _ucp_Batak = 92
const _ucp_Brahmi = 93
const _ucp_Mandaic = 94
const _ucp_Chakma = 95
const _ucp_Meroitic_Cursive = 96
const _ucp_Meroitic_Hieroglyphs = 97
const _ucp_Miao = 98
const _ucp_Sharada = 99
const _ucp_Sora_Sompeng = 100
const _ucp_Takri = 101
const _ucp_Bassa_Vah = 102
const _ucp_Caucasian_Albanian = 103
const _ucp_Duployan = 104
const _ucp_Elbasan = 105
const _ucp_Grantha = 106
const _ucp_Khojki = 107
const _ucp_Khudawadi = 108
const _ucp_Linear_A = 109
const _ucp_Mahajani = 110
const _ucp_Manichaean = 111
const _ucp_Mende_Kikakui = 112
const _ucp_Modi = 113
const _ucp_Mro = 114
const _ucp_Nabataean = 115
const _ucp_Old_North_Arabian = 116
const _ucp_Old_Permic = 117
const _ucp_Pahawh_Hmong = 118
const _ucp_Palmyrene = 119
const _ucp_Psalter_Pahlavi = 120
const _ucp_Pau_Cin_Hau = 121
const _ucp_Siddham = 122
const _ucp_Tirhuta = 123
const _ucp_Warang_Citi = 124

type TBOOL = int32

const _ESC_A = 1
const _ESC_G = 2
const _ESC_K = 3
const _ESC_B = 4
const _ESC_b = 5
const _ESC_D = 6
const _ESC_d = 7
const _ESC_S = 8
const _ESC_s = 9
const _ESC_W = 10
const _ESC_w = 11
const _ESC_N = 12
const _ESC_dum = 13
const _ESC_C = 14
const _ESC_P = 15
const _ESC_p = 16
const _ESC_R = 17
const _ESC_H = 18
const _ESC_h = 19
const _ESC_V = 20
const _ESC_v = 21
const _ESC_X = 22
const _ESC_Z = 23
const _ESC_z = 24
const _ESC_E = 25
const _ESC_Q = 26
const _ESC_g = 27
const _ESC_k = 28
const _ESC_DU = 29
const _ESC_du = 30
const _ESC_SU = 31
const _ESC_su = 32
const _ESC_WU = 33
const _ESC_wu = 34
const _OP_END = 0
const _OP_SOD = 1
const _OP_SOM = 2
const _OP_SET_SOM = 3
const _OP_NOT_WORD_BOUNDARY = 4
const _OP_WORD_BOUNDARY = 5
const _OP_NOT_DIGIT = 6
const _OP_DIGIT = 7
const _OP_NOT_WHITESPACE = 8
const _OP_WHITESPACE = 9
const _OP_NOT_WORDCHAR = 10
const _OP_WORDCHAR = 11
const _OP_ANY = 12
const _OP_ALLANY = 13
const _OP_ANYBYTE = 14
const _OP_NOTPROP = 15
const _OP_PROP = 16
const _OP_ANYNL = 17
const _OP_NOT_HSPACE = 18
const _OP_HSPACE = 19
const _OP_NOT_VSPACE = 20
const _OP_VSPACE = 21
const _OP_EXTUNI = 22
const _OP_EODN = 23
const _OP_EOD = 24
const _OP_DOLL = 25
const _OP_DOLLM = 26
const _OP_CIRC = 27
const _OP_CIRCM = 28
const _OP_CHAR = 29
const _OP_CHARI = 30
const _OP_NOT = 31
const _OP_NOTI = 32
const _OP_STAR = 33
const _OP_MINSTAR = 34
const _OP_PLUS = 35
const _OP_MINPLUS = 36
const _OP_QUERY = 37
const _OP_MINQUERY = 38
const _OP_UPTO = 39
const _OP_MINUPTO = 40
const _OP_EXACT = 41
const _OP_POSSTAR = 42
const _OP_POSPLUS = 43
const _OP_POSQUERY = 44
const _OP_POSUPTO = 45
const _OP_STARI = 46
const _OP_MINSTARI = 47
const _OP_PLUSI = 48
const _OP_MINPLUSI = 49
const _OP_QUERYI = 50
const _OP_MINQUERYI = 51
const _OP_UPTOI = 52
const _OP_MINUPTOI = 53
const _OP_EXACTI = 54
const _OP_POSSTARI = 55
const _OP_POSPLUSI = 56
const _OP_POSQUERYI = 57
const _OP_POSUPTOI = 58
const _OP_NOTSTAR = 59
const _OP_NOTMINSTAR = 60
const _OP_NOTPLUS = 61
const _OP_NOTMINPLUS = 62
const _OP_NOTQUERY = 63
const _OP_NOTMINQUERY = 64
const _OP_NOTUPTO = 65
const _OP_NOTMINUPTO = 66
const _OP_NOTEXACT = 67
const _OP_NOTPOSSTAR = 68
const _OP_NOTPOSPLUS = 69
const _OP_NOTPOSQUERY = 70
const _OP_NOTPOSUPTO = 71
const _OP_NOTSTARI = 72
const _OP_NOTMINSTARI = 73
const _OP_NOTPLUSI = 74
const _OP_NOTMINPLUSI = 75
const _OP_NOTQUERYI = 76
const _OP_NOTMINQUERYI = 77
const _OP_NOTUPTOI = 78
const _OP_NOTMINUPTOI = 79
const _OP_NOTEXACTI = 80
const _OP_NOTPOSSTARI = 81
const _OP_NOTPOSPLUSI = 82
const _OP_NOTPOSQUERYI = 83
const _OP_NOTPOSUPTOI = 84
const _OP_TYPESTAR = 85
const _OP_TYPEMINSTAR = 86
const _OP_TYPEPLUS = 87
const _OP_TYPEMINPLUS = 88
const _OP_TYPEQUERY = 89
const _OP_TYPEMINQUERY = 90
const _OP_TYPEUPTO = 91
const _OP_TYPEMINUPTO = 92
const _OP_TYPEEXACT = 93
const _OP_TYPEPOSSTAR = 94
const _OP_TYPEPOSPLUS = 95
const _OP_TYPEPOSQUERY = 96
const _OP_TYPEPOSUPTO = 97
const _OP_CRSTAR = 98
const _OP_CRMINSTAR = 99
const _OP_CRPLUS = 100
const _OP_CRMINPLUS = 101
const _OP_CRQUERY = 102
const _OP_CRMINQUERY = 103
const _OP_CRRANGE = 104
const _OP_CRMINRANGE = 105
const _OP_CRPOSSTAR = 106
const _OP_CRPOSPLUS = 107
const _OP_CRPOSQUERY = 108
const _OP_CRPOSRANGE = 109
const _OP_CLASS = 110
const _OP_NCLASS = 111
const _OP_XCLASS = 112
const _OP_REF = 113
const _OP_REFI = 114
const _OP_DNREF = 115
const _OP_DNREFI = 116
const _OP_RECURSE = 117
const _OP_CALLOUT = 118
const _OP_ALT = 119
const _OP_KET = 120
const _OP_KETRMAX = 121
const _OP_KETRMIN = 122
const _OP_KETRPOS = 123
const _OP_REVERSE = 124
const _OP_ASSERT = 125
const _OP_ASSERT_NOT = 126
const _OP_ASSERTBACK = 127
const _OP_ASSERTBACK_NOT = 128
const _OP_ONCE = 129
const _OP_ONCE_NC = 130
const _OP_BRA = 131
const _OP_BRAPOS = 132
const _OP_CBRA = 133
const _OP_CBRAPOS = 134
const _OP_COND = 135
const _OP_SBRA = 136
const _OP_SBRAPOS = 137
const _OP_SCBRA = 138
const _OP_SCBRAPOS = 139
const _OP_SCOND = 140
const _OP_CREF = 141
const _OP_DNCREF = 142
const _OP_RREF = 143
const _OP_DNRREF = 144
const _OP_DEF = 145
const _OP_BRAZERO = 146
const _OP_BRAMINZERO = 147
const _OP_BRAPOSZERO = 148
const _OP_MARK = 149
const _OP_PRUNE = 150
const _OP_PRUNE_ARG = 151
const _OP_SKIP = 152
const _OP_SKIP_ARG = 153
const _OP_THEN = 154
const _OP_THEN_ARG = 155
const _OP_COMMIT = 156
const _OP_FAIL = 157
const _OP_ACCEPT = 158
const _OP_ASSERT_ACCEPT = 159
const _OP_CLOSE = 160
const _OP_SKIPZERO = 161
const _OP_TABLE_LENGTH = 162
const _ERR0 = 0
const _ERR1 = 1
const _ERR2 = 2
const _ERR3 = 3
const _ERR4 = 4
const _ERR5 = 5
const _ERR6 = 6
const _ERR7 = 7
const _ERR8 = 8
const _ERR9 = 9
const _ERR10 = 10
const _ERR11 = 11
const _ERR12 = 12
const _ERR13 = 13
const _ERR14 = 14
const _ERR15 = 15
const _ERR16 = 16
const _ERR17 = 17
const _ERR18 = 18
const _ERR19 = 19
const _ERR20 = 20
const _ERR21 = 21
const _ERR22 = 22
const _ERR23 = 23
const _ERR24 = 24
const _ERR25 = 25
const _ERR26 = 26
const _ERR27 = 27
const _ERR28 = 28
const _ERR29 = 29
const _ERR30 = 30
const _ERR31 = 31
const _ERR32 = 32
const _ERR33 = 33
const _ERR34 = 34
const _ERR35 = 35
const _ERR36 = 36
const _ERR37 = 37
const _ERR38 = 38
const _ERR39 = 39
const _ERR40 = 40
const _ERR41 = 41
const _ERR42 = 42
const _ERR43 = 43
const _ERR44 = 44
const _ERR45 = 45
const _ERR46 = 46
const _ERR47 = 47
const _ERR48 = 48
const _ERR49 = 49
const _ERR50 = 50
const _ERR51 = 51
const _ERR52 = 52
const _ERR53 = 53
const _ERR54 = 54
const _ERR55 = 55
const _ERR56 = 56
const _ERR57 = 57
const _ERR58 = 58
const _ERR59 = 59
const _ERR60 = 60
const _ERR61 = 61
const _ERR62 = 62
const _ERR63 = 63
const _ERR64 = 64
const _ERR65 = 65
const _ERR66 = 66
const _ERR67 = 67
const _ERR68 = 68
const _ERR69 = 69
const _ERR70 = 70
const _ERR71 = 71
const _ERR72 = 72
const _ERR73 = 73
const _ERR74 = 74
const _ERR75 = 75
const _ERR76 = 76
const _ERR77 = 77
const _ERR78 = 78
const _ERR79 = 79
const _ERR80 = 80
const _ERR81 = 81
const _ERR82 = 82
const _ERR83 = 83
const _ERR84 = 84
const _ERR85 = 85
const _ERR86 = 86
const _ERR87 = 87
const _ERRCOUNT = 88
const _JIT_COMPILE = 0
const _JIT_PARTIAL_SOFT_COMPILE = 1
const _JIT_PARTIAL_HARD_COMPILE = 2
const _JIT_NUMBER_OF_COMPILE_MODES = 3

type Treal_pcre8_or_16 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Treal_pcre = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Treal_pcre16 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint16
	Freq_char          Tpcre_uint16
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy1            Tpcre_uint16
	Fdummy2            Tpcre_uint16
	Fdummy3            Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type Treal_pcre32 = struct {
	Fmagic_number      Tpcre_uint32
	Fsize              Tpcre_uint32
	Foptions           Tpcre_uint32
	Fflags             Tpcre_uint32
	Flimit_match       Tpcre_uint32
	Flimit_recursion   Tpcre_uint32
	Ffirst_char        Tpcre_uint32
	Freq_char          Tpcre_uint32
	Fmax_lookbehind    Tpcre_uint16
	Ftop_bracket       Tpcre_uint16
	Ftop_backref       Tpcre_uint16
	Fname_table_offset Tpcre_uint16
	Fname_entry_size   Tpcre_uint16
	Fname_count        Tpcre_uint16
	Fref_count         Tpcre_uint16
	Fdummy             Tpcre_uint16
	Ftables            uintptr
	Fnullpad           uintptr
}

type t__assert_real_pcre_size_divisible_8 = [1]int32

type Tpcre_study_data = struct {
	Fsize       Tpcre_uint32
	Fflags      Tpcre_uint32
	Fstart_bits [32]Tpcre_uint8
	Fminlength  Tpcre_uint32
}

type Topen_capitem = struct {
	Fnext   uintptr
	Fnumber Tpcre_uint16
	Fflag   Tpcre_uint16
}

type Tnamed_group = struct {
	Fname   uintptr
	Flength int32
	Fnumber Tpcre_uint32
}

type Tcompile_data = struct {
	Flcc                   uintptr
	Ffcc                   uintptr
	Fcbits                 uintptr
	Fctypes                uintptr
	Fstart_workspace       uintptr
	Fstart_code            uintptr
	Fstart_pattern         uintptr
	Fend_pattern           uintptr
	Fhwm                   uintptr
	Fopen_caps             uintptr
	Fnamed_groups          uintptr
	Fname_table            uintptr
	Fnames_found           int32
	Fname_entry_size       int32
	Fnamed_group_list_size int32
	Fworkspace_size        int32
	Fbracount              uint32
	Ffinal_bracount        int32
	Fmax_lookbehind        int32
	Ftop_backref           int32
	Fbackref_map           uint32
	Fnamedrefcount         uint32
	Fparens_depth          int32
	Fassert_depth          int32
	Fexternal_options      Tpcre_uint32
	Fexternal_flags        Tpcre_uint32
	Freq_varyopt           int32
	Fhad_accept            TBOOL
	Fhad_pruneorskip       TBOOL
	Fcheck_lookbehind      TBOOL
	Fdupnames              TBOOL
	Fdupgroups             TBOOL
	Fiscondassert          TBOOL
	Fnltype                int32
	Fnllen                 int32
	Fnl                    [4]Tpcre_uchar
}

type Tbranch_chain = struct {
	Fouter          uintptr
	Fcurrent_branch uintptr
}

type Trecurse_check = struct {
	Fprev  uintptr
	Fgroup uintptr
}

type Trecursion_info = struct {
	Fprevrec            uintptr
	Fgroup_num          uint32
	Foffset_save        uintptr
	Fsaved_max          int32
	Fsaved_capture_last int32
	Fsubject_position   uintptr
}

type Tdfa_recursion_info = struct {
	Fprevrec          uintptr
	Fgroup_num        int32
	Fsubject_position uintptr
}

type Teptrblock = struct {
	Fepb_prev       uintptr
	Fepb_saved_eptr uintptr
}

type Tmatch_data = struct {
	Fmatch_call_count      uint64
	Fmatch_limit           uint64
	Fmatch_limit_recursion uint64
	Foffset_vector         uintptr
	Foffset_end            int32
	Foffset_max            int32
	Fnltype                int32
	Fnllen                 int32
	Fname_count            int32
	Fname_entry_size       int32
	Fskip_arg_count        uint32
	Fignore_skip_arg       uint32
	Fname_table            uintptr
	Fnl                    [4]Tpcre_uchar
	Flcc                   uintptr
	Ffcc                   uintptr
	Fctypes                uintptr
	Fnotbol                TBOOL
	Fnoteol                TBOOL
	Futf                   TBOOL
	Fjscript_compat        TBOOL
	Fuse_ucp               TBOOL
	Fendonly               TBOOL
	Fnotempty              TBOOL
	Fnotempty_atstart      TBOOL
	Fhitend                TBOOL
	Fbsr_anycrlf           TBOOL
	Fhasthen               TBOOL
	Fstart_code            uintptr
	Fstart_subject         uintptr
	Fend_subject           uintptr
	Fstart_match_ptr       uintptr
	Fend_match_ptr         uintptr
	Fstart_used_ptr        uintptr
	Fpartial               int32
	Fend_offset_top        int32
	Fcapture_last          Tpcre_int32
	Fstart_offset          int32
	Fmatch_function_type   int32
	Feptrchain             uintptr
	Feptrn                 int32
	Frecursive             uintptr
	Fcallout_data          uintptr
	Fmark                  uintptr
	Fnomatch_mark          uintptr
	Fonce_target           uintptr
}

type Tdfa_match_data = struct {
	Fstart_code     uintptr
	Fstart_subject  uintptr
	Fend_subject    uintptr
	Fstart_used_ptr uintptr
	Ftables         uintptr
	Fstart_offset   int32
	Fmoptions       int32
	Fpoptions       int32
	Fnltype         int32
	Fnllen          int32
	Fnl             [4]Tpcre_uchar
	Fcallout_data   uintptr
	Frecursive      uintptr
}

type Tucp_type_table = struct {
	Fname_offset Tpcre_uint16
	Ftype1       Tpcre_uint16
	Fvalue       Tpcre_uint16
}

type Tucd_record = struct {
	Fscript     Tpcre_uint8
	Fchartype   Tpcre_uint8
	Fgbprop     Tpcre_uint8
	Fcaseset    Tpcre_uint8
	Fother_case Tpcre_int32
}

const _REG_ASSERT = 1
const _REG_BADBR = 2
const _REG_BADPAT = 3
const _REG_BADRPT = 4
const _REG_EBRACE = 5
const _REG_EBRACK = 6
const _REG_ECOLLATE = 7
const _REG_ECTYPE = 8
const _REG_EESCAPE = 9
const _REG_EMPTY = 10
const _REG_EPAREN = 11
const _REG_ERANGE = 12
const _REG_ESIZE = 13
const _REG_ESPACE = 14
const _REG_ESUBREG = 15
const _REG_INVARG = 16
const _REG_NOMATCH = 17

type Tregex_t = struct {
	Fre_pcre      uintptr
	Fre_nsub      Tsize_t
	Fre_erroffset Tsize_t
}

type Tregoff_t = int32

type Tregmatch_t = struct {
	Frm_so Tregoff_t
	Frm_eo Tregoff_t
}

/* Table to translate PCRE compile time error codes into POSIX error codes. */

var _eint = [88]int32{
	1:  int32(_REG_EESCAPE),
	2:  int32(_REG_EESCAPE),
	3:  int32(_REG_EESCAPE),
	4:  int32(_REG_BADBR),
	5:  int32(_REG_BADBR),
	6:  int32(_REG_EBRACK),
	7:  int32(_REG_ECTYPE),
	8:  int32(_REG_ERANGE),
	9:  int32(_REG_BADRPT),
	10: int32(_REG_BADRPT),
	11: int32(_REG_ASSERT),
	12: int32(_REG_BADPAT),
	13: int32(_REG_BADPAT),
	14: int32(_REG_EPAREN),
	15: int32(_REG_ESUBREG),
	16: int32(_REG_INVARG),
	17: int32(_REG_INVARG),
	18: int32(_REG_EPAREN),
	19: int32(_REG_ESIZE),
	20: int32(_REG_ESIZE),
	21: int32(_REG_ESPACE),
	22: int32(_REG_EPAREN),
	23: int32(_REG_ASSERT),
	24: int32(_REG_BADPAT),
	25: int32(_REG_BADPAT),
	26: int32(_REG_BADPAT),
	27: int32(_REG_BADPAT),
	28: int32(_REG_BADPAT),
	29: int32(_REG_BADPAT),
	30: int32(_REG_ECTYPE),
	31: int32(_REG_BADPAT),
	32: int32(_REG_INVARG),
	33: int32(_REG_BADPAT),
	34: int32(_REG_BADPAT),
	35: int32(_REG_BADPAT),
	36: int32(_REG_BADPAT),
	37: int32(_REG_EESCAPE),
	38: int32(_REG_BADPAT),
	39: int32(_REG_BADPAT),
	40: int32(_REG_BADPAT),
	41: int32(_REG_BADPAT),
	42: int32(_REG_BADPAT),
	43: int32(_REG_BADPAT),
	44: int32(_REG_BADPAT),
	45: int32(_REG_BADPAT),
	46: int32(_REG_BADPAT),
	47: int32(_REG_BADPAT),
	48: int32(_REG_BADPAT),
	49: int32(_REG_BADPAT),
	50: int32(_REG_BADPAT),
	51: int32(_REG_BADPAT),
	52: int32(_REG_BADPAT),
	53: int32(_REG_BADPAT),
	54: int32(_REG_BADPAT),
	55: int32(_REG_BADPAT),
	56: int32(_REG_INVARG),
	57: int32(_REG_BADPAT),
	58: int32(_REG_BADPAT),
	59: int32(_REG_BADPAT),
	60: int32(_REG_BADPAT),
	61: int32(_REG_BADPAT),
	62: int32(_REG_BADPAT),
	63: int32(_REG_BADPAT),
	64: int32(_REG_BADPAT),
	65: int32(_REG_BADPAT),
	66: int32(_REG_BADPAT),
	67: int32(_REG_INVARG),
	68: int32(_REG_BADPAT),
	69: int32(_REG_BADPAT),
	70: int32(_REG_BADPAT),
	71: int32(_REG_BADPAT),
	72: int32(_REG_BADPAT),
	73: int32(_REG_BADPAT),
	74: int32(_REG_BADPAT),
	75: int32(_REG_BADPAT),
	76: int32(_REG_BADPAT),
	77: int32(_REG_BADPAT),
	78: int32(_REG_BADPAT),
	79: int32(_REG_BADPAT),
	80: int32(_REG_BADPAT),
	81: int32(_REG_BADPAT),
	82: int32(_REG_BADPAT),
	83: int32(_REG_BADPAT),
	84: int32(_REG_BADPAT),
	85: int32(_REG_BADPAT),
	86: int32(_REG_BADPAT),
	87: int32(_REG_BADPAT),
}

/* Table of texts corresponding to POSIX error codes */

var _pstring = [18]uintptr{
	0:  __ccgo_ts,
	1:  __ccgo_ts + 1,
	2:  __ccgo_ts + 16,
	3:  __ccgo_ts + 44,
	4:  __ccgo_ts + 58,
	5:  __ccgo_ts + 72,
	6:  __ccgo_ts + 86,
	7:  __ccgo_ts + 100,
	8:  __ccgo_ts + 131,
	9:  __ccgo_ts + 141,
	10: __ccgo_ts + 161,
	11: __ccgo_ts + 178,
	12: __ccgo_ts + 192,
	13: __ccgo_ts + 212,
	14: __ccgo_ts + 231,
	15: __ccgo_ts + 252,
	16: __ccgo_ts + 271,
	17: __ccgo_ts + 284,
}

/*************************************************
*          Translate error code to string        *
*************************************************/
func Xregerror(tls *libc.TLS, errcode int32, preg uintptr, errbuf uintptr, errbuf_size Tsize_t) (r Tsize_t) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var addlength, length Tsize_t
	var addmessage, message, v1 uintptr
	var v2 uint64
	_, _, _, _, _, _ = addlength, addmessage, length, message, v1, v2
	if errcode >= libc.Int32FromUint64(libc.Uint64FromInt64(144)/libc.Uint64FromInt64(8)) {
		v1 = __ccgo_ts + 297
	} else {
		v1 = _pstring[errcode]
	}
	message = v1
	length = libc.Xstrlen(tls, message) + uint64(1)
	addmessage = __ccgo_ts + 316
	if preg != libc.UintptrFromInt32(0) && libc.Int32FromUint64((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset) != -int32(1) {
		v2 = libc.Xstrlen(tls, addmessage) + uint64(6)
	} else {
		v2 = uint64(0)
	}
	addlength = v2
	if errbuf_size > uint64(0) {
		if addlength > uint64(0) && errbuf_size >= length+addlength {
			libc.Xsprintf(tls, errbuf, __ccgo_ts+328, libc.VaList(bp+8, message, addmessage, libc.Int32FromUint64((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset)))
		} else {
			libc.Xstrncpy(tls, errbuf, message, errbuf_size-uint64(1))
			*(*int8)(unsafe.Pointer(errbuf + uintptr(errbuf_size-uint64(1)))) = 0
		}
	}
	return length + addlength
}

/*************************************************
*           Free store held by a regex           *
*************************************************/
func Xregfree(tls *libc.TLS, preg uintptr) {
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{libpcre.Xpcre_free})))(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre)
}

/*************************************************
*            Compile a regular expression        *
*************************************************/

/*
Arguments:

	preg        points to a structure for recording the compiled expression
	pattern     the pattern to compile
	cflags      compilation flags

Returns:      0 on success

	various non-zero codes on failure
*/
func Xregcomp(tls *libc.TLS, preg uintptr, pattern uintptr, cflags int32) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var options, v1 int32
	var _ /* erroffset at bp+8 */ int32
	var _ /* errorcode at bp+12 */ int32
	var _ /* errorptr at bp+0 */ uintptr
	var _ /* re_nsub at bp+16 */ int32
	_, _ = options, v1
	options = 0
	*(*int32)(unsafe.Pointer(bp + 16)) = 0
	if cflags&int32(m_REG_ICASE) != 0 {
		options |= int32(m_PCRE_CASELESS)
	}
	if cflags&int32(m_REG_NEWLINE) != 0 {
		options |= int32(m_PCRE_MULTILINE)
	}
	if cflags&int32(m_REG_DOTALL) != 0 {
		options |= int32(m_PCRE_DOTALL)
	}
	if cflags&int32(m_REG_NOSUB) != 0 {
		options |= int32(m_PCRE_NO_AUTO_CAPTURE)
	}
	if cflags&int32(m_REG_UTF8) != 0 {
		options |= int32(m_PCRE_UTF8)
	}
	if cflags&int32(m_REG_UCP) != 0 {
		options |= int32(m_PCRE_UCP)
	}
	if cflags&int32(m_REG_UNGREEDY) != 0 {
		options |= int32(m_PCRE_UNGREEDY)
	}
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre = libpcre.Xpcre_compile2(tls, pattern, options, bp+12, bp, bp+8, libc.UintptrFromInt32(0))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = libc.Uint64FromInt32(*(*int32)(unsafe.Pointer(bp + 8)))
	/* Safety: if the error code is too big for the translation vector (which
	   should not happen, but we all make mistakes), return REG_BADPAT. */
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre == libc.UintptrFromInt32(0) {
		if *(*int32)(unsafe.Pointer(bp + 12)) < libc.Int32FromUint64(libc.Uint64FromInt64(352)/libc.Uint64FromInt64(4)) {
			v1 = _eint[*(*int32)(unsafe.Pointer(bp + 12))]
		} else {
			v1 = int32(_REG_BADPAT)
		}
		return v1
	}
	libpcre.Xpcre_fullinfo(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre, libc.UintptrFromInt32(0), int32(m_PCRE_INFO_CAPTURECOUNT), bp+16)
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_nsub = libc.Uint64FromInt32(*(*int32)(unsafe.Pointer(bp + 16)))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = libc.Uint64FromInt32(-libc.Int32FromInt32(1)) /* No meaning after successful compile */
	return 0
}

/*************************************************
*              Match a regular expression        *
*************************************************/

/*
	Unfortunately, PCRE requires 3 ints of working space for each captured

substring, so we have to get and release working store instead of just using
the POSIX structures as was done in earlier releases when PCRE needed only 2
ints. However, if the number of possible capturing brackets is small, use a
block of store on the stack, to reduce the use of malloc/free. The threshold is
in a macro that can be changed at configure time.

If REG_NOSUB was specified at compile time, the PCRE_NO_AUTO_CAPTURE flag will
be set. When this is the case, the nmatch and pmatch arguments are ignored, and
the only result is yes/no/error.
*/
func Xregexec(tls *libc.TLS, preg uintptr, string1 uintptr, nmatch Tsize_t, pmatch uintptr, eflags int32) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var allocated_ovector, nosub TBOOL
	var eo, options, rc, so, v2, v3 int32
	var i Tsize_t
	var ovector uintptr
	var v5 Tregoff_t
	var _ /* small_ovector at bp+0 */ [30]int32
	_, _, _, _, _, _, _, _, _, _, _ = allocated_ovector, eo, i, nosub, options, ovector, rc, so, v2, v3, v5
	options = 0
	ovector = libc.UintptrFromInt32(0)
	allocated_ovector = m_FALSE
	nosub = libc.BoolInt32((*Treal_pcre)(unsafe.Pointer((*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre)).Foptions&uint32(m_PCRE_NO_AUTO_CAPTURE) != uint32(0))
	if eflags&int32(m_REG_NOTBOL) != 0 {
		options |= int32(m_PCRE_NOTBOL)
	}
	if eflags&int32(m_REG_NOTEOL) != 0 {
		options |= int32(m_PCRE_NOTEOL)
	}
	if eflags&int32(m_REG_NOTEMPTY) != 0 {
		options |= int32(m_PCRE_NOTEMPTY)
	}
	/* When no string data is being returned, or no vector has been passed in which
	   to put it, ensure that nmatch is zero. Otherwise, ensure the vector for holding
	   the return data is large enough. */
	if nosub != 0 || pmatch == libc.UintptrFromInt32(0) {
		nmatch = uint64(0)
	} else {
		if nmatch > uint64(0) {
			if nmatch <= uint64(m_POSIX_MALLOC_THRESHOLD) {
				ovector = bp
			} else {
				if nmatch > libc.Uint64FromInt32(m_INT_MAX)/(libc.Uint64FromInt64(4)*libc.Uint64FromInt32(3)) {
					return int32(_REG_ESPACE)
				}
				ovector = libc.Xmalloc(tls, uint64(4)*nmatch*uint64(3))
				if ovector == libc.UintptrFromInt32(0) {
					return int32(_REG_ESPACE)
				}
				allocated_ovector = int32(m_TRUE)
			}
		}
	}
	/* REG_STARTEND is a BSD extension, to allow for non-NUL-terminated strings.
	   The man page from OS X says "REG_STARTEND affects only the location of the
	   string, not how it is matched". That is why the "so" value is used to bump the
	   start location rather than being passed as a PCRE "starting offset". */
	if eflags&int32(m_REG_STARTEND) != 0 {
		if pmatch == libc.UintptrFromInt32(0) {
			return int32(_REG_INVARG)
		}
		so = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_so
		eo = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_eo
	} else {
		so = 0
		eo = libc.Int32FromUint64(libc.Xstrlen(tls, string1))
	}
	rc = libpcre.Xpcre_exec(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre, libc.UintptrFromInt32(0), string1+uintptr(so), eo-so, 0, options, ovector, libc.Int32FromUint64(nmatch*libc.Uint64FromInt32(3)))
	if rc == 0 {
		rc = libc.Int32FromUint64(nmatch)
	} /* All captured slots were filled in */
	/* Successful match */
	if rc >= 0 {
		if !(nosub != 0) {
			i = uint64(0)
			for {
				if !(i < libc.Uint64FromInt32(rc)) {
					break
				}
				if *(*int32)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*4)) < 0 {
					v2 = -int32(1)
				} else {
					v2 = *(*int32)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*4)) + so
				}
				(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v2
				if *(*int32)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*4)) < 0 {
					v3 = -int32(1)
				} else {
					v3 = *(*int32)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*4)) + so
				}
				(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v3
				goto _1
			_1:
				;
				i++
			}
			if allocated_ovector != 0 {
				libc.Xfree(tls, ovector)
			}
			for {
				if !(i < nmatch) {
					break
				}
				v5 = -libc.Int32FromInt32(1)
				(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v5
				(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v5
				goto _4
			_4:
				;
				i++
			}
		}
		return 0
	}
	/* Unsuccessful match */
	if allocated_ovector != 0 {
		libc.Xfree(tls, ovector)
	}
	switch rc {
	/* ========================================================================== */
	/* These cases are never obeyed. This is a fudge that causes a compile-time
	error if the vector eint, which is indexed by compile-time error number, is
	not the correct length. It seems to be the only way to do such a check at
	compile time, as the sizeof() operator does not work in the C preprocessor.
	As all the PCRE_ERROR_xxx values are negative, we can use 0 and 1. */
	case 0:
		fallthrough
	case libc.BoolInt32(libc.Uint64FromInt64(352)/libc.Uint64FromInt64(4) == uint64(_ERRCOUNT)):
		return int32(_REG_ASSERT)
		/* ========================================================================== */
		fallthrough
	case -int32(1):
		return int32(_REG_NOMATCH)
	case -int32(2):
		return int32(_REG_INVARG)
	case -int32(3):
		return int32(_REG_INVARG)
	case -int32(4):
		return int32(_REG_INVARG)
	case -int32(5):
		return int32(_REG_ASSERT)
	case -int32(6):
		return int32(_REG_ESPACE)
	case -int32(8):
		return int32(_REG_ESPACE)
	case -int32(10):
		return int32(_REG_INVARG)
	case -int32(11):
		return int32(_REG_INVARG)
	case -int32(28):
		return int32(_REG_INVARG)
	default:
		return int32(_REG_ASSERT)
	}
	return r
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00internal error\x00invalid repeat counts in {}\x00pattern error\x00? * + invalid\x00unbalanced {}\x00unbalanced []\x00collation error - not relevant\x00bad class\x00bad escape sequence\x00empty expression\x00unbalanced ()\x00bad range inside []\x00expression too big\x00failed to get memory\x00bad back reference\x00bad argument\x00match failed\x00unknown error code\x00 at offset \x00%s%s%-6d\x00"
