// Copyright 2023 The libpcreposix-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libpcreposix is a ccgo/v4 version of libpcreposix.a (https://www.pcre.org)
package libpcreposix // import "modernc.org/libpcreposix"
