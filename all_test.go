// Copyright 2023 The libpcreposix-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libpcreposix // import "modernc.org/libpcreposix"

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
	"time"

	_ "modernc.org/libc"
	util "modernc.org/ccgo/v3/lib"
	_ "modernc.org/ccgo/v4/lib"
)

var (
	oXTags = flag.String("xtags", "", "passed as -tags to go build of testfixture")
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

func Test(t *testing.T) {
	tmpDir := t.TempDir()
	bin := filepath.Join(tmpDir, "pcretest")
	if out, err := util.Shell("go", "build", "-o", bin, "-tags="+*oXTags, filepath.Join("internal", "pcretest", fmt.Sprintf("ccgo_%s_%s.go", runtime.GOOS, runtime.GOARCH))); err != nil {
		t.Fatalf("%s\nFAIL: %v", out, err)
	}

	if _, _, err := util.CopyDir(filepath.Join(tmpDir, "testdata"), filepath.Join("internal", "testdata"), nil); err != nil {
		t.Fatal(err)
	}

	if _, err := util.CopyFile(filepath.Join(tmpDir, "RunTest"), filepath.Join("internal", "RunTest"), nil); err != nil {
		t.Fatal(err)
	}

	if err := util.InDir(tmpDir, func() error {
		t0 := time.Now()
		out, err := util.Shell("./RunTest")
		t.Logf("time: %v", time.Since(t0))
		for _, v := range strings.Split(string(out), "\n") {
			if strings.Contains(strings.ToLower(v), "fail") {
				t.Error(v)
			}
		}
		return err
	}); err != nil {
		t.Fatal(err)
	}
}
